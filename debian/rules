#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
export DEB_CXXFLAGS_MAINT_APPEND  = -Wall -pedantic

# trust upstream optimization level by default
ifeq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
export DEB_CFLAGS_MAINT_STRIP=-O2
export DEB_CXXFLAGS_MAINT_STRIP=-O2
endif

# resolve all direct entries in first "package..." section in TODO
VENDORLIBS = $(shell perl -nE \
 '/^(?:\s{2}[*]\s*(?:(package)|(\S+))|\s{4}[*]\s*(\S+))/ or next; \
 $$i++ if $$1; exit if $$i > 1 or $$2; say $$3 if $$3 and $$i;' \
 debian/TODO)

# have embedded crate librocksdb-sys use system library
export ROCKSDB_INCLUDE_DIR = /usr/include

comma = ,
_mangle = perl -gpi \
 -e 's/$(1)(\W+version)? = "\K\Q$(subst |,$(comma),$(2))"/$(subst |,$(comma),$(3))"/g;' \
 $(patsubst %,debian/vendorlibs/%/Cargo.toml,$(4))

%:
	dh $@

execute_before_dh_auto_configure:
	[ -f Cargo.toml.orig ] || cp Cargo.toml Cargo.toml.orig
	cp -f debian/Cargo.toml Cargo.toml

# TODO: drop this dirty hack when crates rust-librocksdb-sys is in Debian
override_dh_auto_install:
	dh_auto_install --buildsystem rust || true

execute_after_dh_install:
	mv debian/matrix-conduit/etc/matrix-conduit/conduit-example.toml debian/matrix-conduit/etc/matrix-conduit/conduit.toml
	mv debian/matrix-conduit/usr/sbin/conduit debian/matrix-conduit/usr/sbin/matrix-conduit
	sed -i \
	 -e 's,^#server_name\s.*=.*,server_name = "localhost",' \
	 -e 's,^database_path\s*=.*,database_path = "/var/lib/matrix-conduit/db",' \
	 debian/matrix-conduit/etc/matrix-conduit/conduit.toml

# custom target unused during official build
get-vendorlibs:
# preserve and use pristine Cargo.toml and Cargo.lock
	[ -f Cargo.lock.orig ] || cp Cargo.lock Cargo.lock.orig
	cp -f Cargo.lock.orig Cargo.lock

# modernize crates
	cargo update

# preserve needed crates
	cargo vendor --versioned-dirs debian/vendorlibs~
	rm -rf debian/vendorlibs
	mkdir debian/vendorlibs
	cd debian/vendorlibs~ && mv --target-directory=../vendorlibs $(VENDORLIBS)

# tolerate binary files in preserved code
	find debian/vendorlibs -type f ! -size 0 | perl -lne 'print if -B' \
	 > debian/source/include-binaries

# accept newer crates
	$(call _mangle,base64,0.21.0,>= 0.21.0| <= 0.22,ruma-common-0.12.1 ruma-signatures-0.14.0)
	$(call _mangle,itertools,0.11.0,>= 0.11.0| <= 0.13,ruma-state-res-0.10.0)
	$(call _mangle,proc-macro-crate,2.0.0,>= 2| <= 3,ruma-macros-0.12.0)

# accept older crates
	$(call _mangle,bindgen,0.69,>= 0.66| <= 0.69,rust-librocksdb-sys-0.21.0+9.1.1)
	$(call _mangle,proc-macro-crate,2.0.0,1.3.1,ruma-macros-0.12.0)
	$(call _mangle,spki,0.7.3,0.7.2,ruma-macros-0.12.0)
	$(call _mangle,time,0.3.34,0.3.31,ruma-common-0.12.1)

# avoid crate web-time
	perl -gpi \
	 -e 's/\n\[dependencies.web-time\]\nversion = "1.1.0"\n\n/\n\n/g;' \
	 debian/vendorlibs/ruma-0.9.4/Cargo.toml \
	 debian/vendorlibs/ruma-common-0.12.1/Cargo.toml
	perl -gpi \
	 -e 's/use web_time as time/use std::time/;' \
	 debian/vendorlibs/ruma-0.9.4//src/lib.rs
	perl -gpi \
	 -e 's/use web_time::/use std::time::/;' \
	 debian/vendorlibs/ruma-common-0.12.1/src/time.rs

# avoid libz-sys feature "static"
	perl -gpi \
	 -e 's/\ndefault = \[\K"static"//;' \
	 -e 's/\nstatic = \[[^\[\]]+\]//;' \
	 debian/vendorlibs/rust-librocksdb-sys-0.21.0+9.1.1/Cargo.toml

# avoid WASM-only crates
	perl -gpi \
	 -e 's/\[target\w+cfg(.all)?.target_(arch|family)\W+wasm[^\n]*(\n[^\[\n][^\n]*)*//g;' \
	 debian/vendorlibs/ruma-common-0.12.1/Cargo.toml
	perl -gpi \
	 -e 's/\njs = \[[^\]]+\]//g;' \
	 debian/vendorlibs/ruma-common-0.12.1/Cargo.toml

# strip checksums for mangled crates
	echo '{"package":"Could not get crate checksum","files":{}}' \
	 | tee $(patsubst %,debian/vendorlibs/%/.cargo-checksum.json,\
	 ruma-0.9.4 \
	 ruma-common-0.12.1 \
	 ruma-macros-0.12.0 \
	 ruma-signatures-0.14.0 \
	 ruma-state-res-0.10.0 \
	 rust-librocksdb-sys-0.21.0+9.1.1 \
	 )

	$(eval GONE = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is missing' debian/TODO)))
	$(eval PENDING = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is pending' debian/TODO)))
	$(eval INCOMPLETE = $(filter-out 0,\
	 $(shell grep -Fc '      * package lacks feature' debian/TODO)))
	$(eval BROKEN = $(filter-out 0,\
	 $(shell grep -c '      \* package is .*broken' debian/TODO)))
	$(eval UNWANTED = $(filter-out 0,\
	 $(shell grep -c '      \* package is unwanted' debian/TODO)))
	$(eval OLD = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is outdated' debian/TODO)))
	$(eval AHEAD = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is ahead' debian/TODO)))
	$(eval SNAPSHOT = $(filter-out 0,\
	 $(shell grep -Fxc '      * package should cover snapshot' debian/TODO)))
	$(eval REASONS = $(strip\
	 $(if $(GONE),$(GONE)_missing)\
	 $(if $(PENDING),$(PENDING)_pending)\
	 $(if $(BROKEN),$(BROKEN)_broken)\
	 $(if $(UNWANTED),$(UNWANTED)_unwanted)\
	 $(if $(INCOMPLETE),$(INCOMPLETE)_incomplete)\
	 $(if $(OLD),$(OLD)_outdated)\
	 $(if $(AHEAD),$(AHEAD)_ahead)\
	 $(if $(SNAPSHOT),$(SNAPSHOT)_unreleased)))
	$(eval c = ,)
	$(eval EMBEDDED = $(if $(REASONS),\n    ($(subst _,$() ,$(subst $() ,$c_,$(REASONS))))))
	@CRATES=$$(ls -1 debian/vendorlibs | grep -c .);\
	perl -gpi \
	 -e 's/FIXME: avoid most possible of embedded \K\d+ crates\n\h+[^\)\n]*\)/'"$$CRATES"' crates$(EMBEDDED)/;' \
	 debian/changelog;\
	echo;\
	echo "DONE: embedding $$CRATES crates$(EMBEDDED)";\
	echo

# preserve mangled Cargo.lock for use during build
	grep -v -e '^checksum ' Cargo.lock \
	 | perl -pe 's,^source = "\Kgit[^"]+,registry+https://github.com/rust-lang/crates.io-index,' \
	 > debian/Cargo.lock

# preserve mangled Cargo.toml for use during build
	perl -gp \
	 -e 's/ruma\W+\Kgit = \S+\s+rev = "[^"]+"/version = "0.9.4"/;' \
	 Cargo.toml \
	 > debian/Cargo.toml

	mv -f Cargo.lock.orig Cargo.lock
